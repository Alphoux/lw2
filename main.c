#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

char pressed = 1;
char pushed = 0;
char pressed2 = 0;
char count = 0;

int main(void){
  DDRC = 0xFF;
  DDRB = 0b11111100;
  DDRD = 0;
  PORTD = 0x00;
  PORTB = 0x00;
  PORTC = 0;
  PCICR |= (1 << PCIE2)|(1<<PCIE0);
  PCMSK2 |= (1 << PCINT20)|(1<<PCINT19);
  PCMSK0 |= (1 << PCINT0);
  sei();
  mandatory_part();
  additional_part();
  cli();
}

ISR(PCINT2_vect){
  if(pushed == 0){
    count=(count+1)%8;
    PORTC = count;
    pushed = 1;
  }
  else{
    pushed = 0;
  }
  if (pressed == 1){
    pressed =0;
  }
  else{
    pressed =1;
  }
}

ISR(PCINT0_vect){
  if(pressed2 == 0){
    PORTB = PORTB |(1<<5);
    pressed2 = 1;
  }
  else
  {
    PORTB = PORTB&0b11011111;
  	pressed2 =0;
  }
}


int mandatory_part(void){
  	while(1){
    }
}

int additional_part(void){
    char count2 = 0;
  	
    while(1){
      if(count2%3 == 2){
        PORTC = 1<<5;
      }
      else{
        if(count2%3 == 1){
          PORTC = 1<<4;
        }
        else{
          PORTC = 1<<3;
        }
      }
      if (pressed == 1){
      	count2 ++;
      }
      _delay_ms(250);
    }
}